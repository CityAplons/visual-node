## Ultrabot visual system

### Build requirements

ROS Packages:
+ cv_bridge
+ realsense-ros
+ depth_image_proc
+ ultrabot_camera
+ multimaster
+ robot_upstart

C/C++ libraries:
+ PCL
+ CUDA
+ realsensesdk2.0
+ BOOST

Python 3

Build command:
```
catkin_make -DCMAKE_BUILD_TYPE=Release -DPYTHON_EXECUTABLE=/usr/bin/python3 -DPYTHON_INCLUDE_DIR=/usr/include/python3.6m -DPYTHON_LIBRARY=/usr/lib/aarch64-linux-gnu/libpython3.6m.so
```

