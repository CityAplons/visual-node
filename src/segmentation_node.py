#!/usr/bin/env python3
from numpy.core.numeric import argwhere
import rospy
from sensor_msgs.msg import Image as msg_Image
from sensor_msgs.msg import CameraInfo
from std_msgs.msg import Int32
from std_msgs.msg import Float32
from cv_bridge import CvBridge
from ultrabot_camera.msg import hfloat
from message_filters import TimeSynchronizer, Subscriber
import numpy as np
import cv2

# import time

import sys
print("Python v",sys.version)

import pycuda.autoinit
from utils.yolo_with_plugins import TrtYOLO
import threading

# import pyrealsense2 as rs2
# if (not hasattr(rs2, 'intrinsics')):
#     import pyrealsense2.pyrealsense2 as rs2

class yolo_detector:
    def __init__(self, id):
        self.mutex = threading.Lock()
        print("Warming up detector with id: ", id)
        self.net = TrtYOLO("yolov4-tiny-3l-crowdhuman-416x416", 2, letter_box=True, cuda_ctx=pycuda.autoinit.context)
        print("Detector warmed up!")

    def detect(self, image):
        self.mutex.acquire()
        boxes, scores, classes = self.net.detect(image, 0.31)
        self.mutex.release()
        return boxes, scores, classes

class HumanSegmenter:
    def __init__(self,topic,inference,rot):
        self.bridge = CvBridge()
        self.net = inference
        self.rotate = rot
        self.count = 0
        self.av_conf = 0
        self.depth_intrinsic = None
        self.color_intrinsic = None
        self.width = 0
        self.height = 0
        self.tf = np.matrix([[ 1.000, 0.006, 0.004, -0.015],
                               [-0.006, 1.000,-0.002, 0.00],
                               [-0.004, 0.002, 1.000, 0.00],
                               [ 0.000, 0.000, 0.000, 1.00]])
        # self.inf_time = []
        # self.cv_time = []
        # self.extr_time = []
        print("HumanSegmenter created.")

        self.subDepth = Subscriber('/'+topic+'/depth/image_rect_raw', msg_Image)
        self.subRGB = Subscriber('/'+topic+'/color/image_raw', msg_Image)
        self.subDepth_info = Subscriber('/'+topic+'/depth/camera_info', CameraInfo)
        self.subColor_info = Subscriber('/'+topic+'/color/camera_info', CameraInfo)
        self.ats = TimeSynchronizer([self.subDepth, self.subDepth_info, self.subRGB, self.subColor_info], queue_size=3)
        self.ats.registerCallback(self.processFrameSet)

        self.pubCount = rospy.Publisher(topic+'/human/count', Int32, queue_size=1)
        self.pubConf = rospy.Publisher(topic+'/human/confidence', hfloat, queue_size=1)
        self.pub = rospy.Publisher(topic+'/human/segmented', msg_Image, queue_size=1)
        self.pubInv = rospy.Publisher(topic+'/human/segmented_inv', msg_Image, queue_size=1)
        self.pubInfo = rospy.Publisher(topic+'/human/camera_info', CameraInfo, queue_size=1)
        #self.rate = rospy.Rate(8)

    def processFrameSet(self, depth, depth_info, rgb, rgb_info):
        #if not rospy.is_shutdown():
        #start = time.time()

        depth_cv = self.bridge.imgmsg_to_cv2(depth, desired_encoding='16UC1')
        rgb_cv = self.bridge.imgmsg_to_cv2(rgb, desired_encoding='rgb8')

        # self.cv_time.append(time.time()-start)
        # start = time.time()

        self.height = depth_cv.shape[0]
        self.width = depth_cv.shape[1]
        self.depth_intrinsic = depth_info.K
        self.color_intrinsic = rgb_info.K

        if self.rotate >= 1:
           rgb_cv = cv2.rotate(rgb_cv, cv2.ROTATE_90_COUNTERCLOCKWISE)
           depth_cv = cv2.rotate(depth_cv, cv2.ROTATE_90_COUNTERCLOCKWISE)
           self.height = depth_cv.shape[0]
           self.width = depth_cv.shape[1]
        elif self.rotate <= -1:
           rgb_cv = cv2.rotate(rgb_cv, cv2.ROTATE_90_CLOCKWISE)
           depth_cv = cv2.rotate(depth_cv, cv2.ROTATE_90_CLOCKWISE)
           self.height = depth_cv.shape[0]
           self.width = depth_cv.shape[1]
        boxes, conf, clss = self.net.detect(rgb_cv)
        # self.inf_time.append(time.time()-start)
        # start = time.time()

        mask = self.get_mask(depth_cv, boxes, clss, conf)
        # self.extr_time.append(time.time()-start)
        # start = time.time()

        if self.rotate >= 1:
           depth_cv = cv2.rotate(depth_cv, cv2.ROTATE_90_CLOCKWISE)
           mask = np.rot90(mask,3)
        elif self.rotate <= -1:
           depth_cv = cv2.rotate(depth_cv, cv2.ROTATE_90_COUNTERCLOCKWISE)
           mask = np.rot90(mask,1)

        mask_cv = np.uint8(mask*255)

        dpth = cv2.bitwise_and(depth_cv, depth_cv, mask = mask_cv)
        inv = cv2.bitwise_and(depth_cv, depth_cv, mask = (255-mask_cv))

        dpth_msg = self.bridge.cv2_to_imgmsg(dpth,encoding='16UC1')
        dpth_msg.header.stamp = depth.header.stamp
        inv_msg = self.bridge.cv2_to_imgmsg(inv,encoding='16UC1')
        inv_msg.header.stamp = depth.header.stamp

        hfloat_obj = hfloat()
        hfloat_obj.value = self.av_conf
        hfloat_obj.stamp = rospy.get_rostime()
        self.pubConf.publish(hfloat_obj)
        self.pubInv.publish(dpth_msg)
        self.pub.publish(inv_msg)
        self.pubCount.publish(self.count)
        self.pubInfo.publish(depth_info)

        # if len(self.inf_time) >= 1000:
        #     print("=== 1000 sample timings ===")
        #     cv_std = np.std(self.cv_time)
        #     inf_std = np.std(self.inf_time)
        #     extr_std = np.std(self.extr_time)
        #     print("CV proc: ", np.average(self.cv_time), "±", cv_std)
        #     print("Inference: ", np.average(self.inf_time), "±", inf_std)
        #     print("Mask extr: ", np.average(self.extr_time), "±", extr_std)
        #     self.cv_time = []
        #     self.inf_time = []
        #     self.extr_time = []
            #self.rate.sleep()

    def transform(self, bbox, tf, depth):
        new_bbox = bbox
        if (self.color_intrinsic or self.depth_intrinsic) != None and depth != 0:
            # Project bbox to 3D Space
            p1_x = (bbox[0]-self.color_intrinsic[2])/self.color_intrinsic[0] * depth
            p1_y = (bbox[1]-self.color_intrinsic[5])/self.color_intrinsic[4] * depth
            p2_x = (bbox[2]-self.color_intrinsic[2])/self.color_intrinsic[0] * depth
            p2_y = (bbox[3]-self.color_intrinsic[5])/self.color_intrinsic[4] * depth
            p1 = np.array([p1_x,p1_y,depth,1]).T
            p2 = np.array([p2_x,p2_y,depth,1]).T
            # Transform points to depth frame
            p1 = np.dot(p1,tf)[:,:3]
            p2 = np.dot(p2,tf)[:,:3]
            # De-project to 2D space in depth frame
            intr = (np.asarray(np.split(np.asarray(self.depth_intrinsic),3)))
            p1 = np.dot(intr,p1.T)/depth
            p2 = np.dot(intr,p2.T)/depth

            if(p1[0] >= self.width):
                p1[0] = self.width - 1
            elif p1[0] < 0:
                p1[0] = 0

            if(p2[0] >= self.width):
                p2[0] = self.width - 1
            elif p2[0] < 0:
                p2[0] = 0
            
            if(p1[1] >= self.height):
                p1[1] = self.height - 1
            elif p1[1] < 0:
                p1[1] = 0
            
            if(p2[1] >= self.height):
                p2[1] = self.height - 1
            elif p2[1] < 0:
                p2[1] = 0

            new_bbox = [int(min(p1[0],p2[0])),int(min(p1[1],p2[1])),int(max(p1[0],p2[0])),int(max(p1[1],p2[1]))]
        return new_bbox

    def get_mask(self, depth, bboxes, classes, conf):
        color_to_depth = self.tf
        mask = np.ones(depth.shape, dtype=bool)
        #mask = np.zeros(depth.shape, dtype="uint8")

        # find humans
        self.count = 0
        self.av_conf = 0
        if len(classes) > 0:
            ids = np.argwhere(classes == 1)
            self.count = len(ids)
            if self.count > 0:
                self.av_conf = np.max(conf[ids])
        else:
            return mask
    
        for i in ids:
            box = bboxes[i][0]
            box = self.transform(box, color_to_depth, 50)
            cy = int(box[1] + (box[3] - box[1])/2)
            cx = int(box[0] + (box[2] - box[0])/2)
                
            mask[box[1]:box[3],box[0]:box[2]] = 0
            kernel = np.ones((5,5), np.uint8)
            depth8 = cv2.normalize(depth, None, 0, 255, cv2.NORM_MINMAX, dtype=cv2.CV_8U)
            _,th_near = cv2.threshold(depth8,depth8[cy,cx]+2,1,cv2.THRESH_BINARY)
            th_near = cv2.dilate(th_near, kernel)
            th_near = cv2.erode(th_near, kernel)

            _,th_far = cv2.threshold(depth8,depth8[cy,cx]-2,1,cv2.THRESH_BINARY)
            th_far[th_far==0] = 255
            th_far = cv2.dilate(th_far, kernel)
            th_far = cv2.erode(th_far, kernel)
            
            th = cv2.bitwise_and(th_near,th_far)

            mask[box[1]:box[3],box[0]:box[2]] = th[box[1]:box[3],box[0]:box[2]]>0
        return mask


def main():
    rospy.init_node("segmentation")

    if not (rospy.has_param('~camera') or rospy.has_param('~rot')):
        print("Camera topics were not provided. Shutting down...")
        return
    
    topic = rospy.get_param('~camera')
    rotation = rospy.get_param('~rot')

    print("Creating YOLOv4 instances...")
    inf = yolo_detector(topic)

    print("Subscribing to topic: ", topic)

    node = HumanSegmenter(topic, inf, rotation)
    rospy.spin()

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
