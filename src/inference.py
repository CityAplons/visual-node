#!/usr/bin/env python3

import pycuda.autoinit
from utils.yolo_with_plugins import get_input_shape, TrtYOLO
import threading


class yolo_detector:
    def __init__(self, id):
        self.mutex = threading.Lock()
        print("Warming up detector with id: ", id)
        self.net = TrtYOLO("yolov4-tiny-3l-crowdhuman-416x416", 2, letter_box=True, cuda_ctx=pycuda.autoinit.context)
        print("Detector warmed up!")

    def detect(self, image):
        self.mutex.acquire()
        boxes, scores, classes = self.net.detect(image, 0.34)
        self.mutex.release()
        return boxes, scores, classes